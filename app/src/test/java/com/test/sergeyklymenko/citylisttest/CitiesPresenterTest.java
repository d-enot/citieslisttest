package com.test.sergeyklymenko.citylisttest;

import android.support.annotation.NonNull;

import com.test.sergeyklymenko.citylisttest.citieslist.CitiesContract;
import com.test.sergeyklymenko.citylisttest.citieslist.CitiesPresenter;
import com.test.sergeyklymenko.citylisttest.data.CitiesRepository;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static com.test.sergeyklymenko.citylisttest.TestUtil.getCities;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CitiesPresenterTest {

    @Mock
    CitiesRepository getCitiesUseCase;

    @Mock
    CitiesContract.View view;

    private CitiesPresenter presenter;

    @BeforeClass
    public static void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run,
                                             long delay,
                                             @NonNull TimeUnit unit) {
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Scheduler.Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new CitiesPresenter(getCitiesUseCase);
        presenter.takeView(view);
    }

    @Test
    public void getCitiesTest() {
        when(view.networkEnabled()).thenReturn(true);
        when(getCitiesUseCase.getCities()).thenReturn(Flowable.just(getCities()));

        presenter.getCities();

        verify(view).hideLoading();
        verify(view).showCities(any());
    }

    @Test
    public void getCitiesTest_networkError() {
        when(view.networkEnabled()).thenReturn(false);
        when(getCitiesUseCase.getCities()).thenReturn(Flowable.just(getCities()));

        presenter.getCities();

        verify(view).showNetworkError();
    }

    @Test
    public void getCitiesTest_Error() {
        when(view.networkEnabled()).thenReturn(true);
        when(getCitiesUseCase.getCities()).thenReturn(Flowable.error(new Throwable("Test error")));

        presenter.getCities();

        verify(view).showError(any(String.class));
    }
}
