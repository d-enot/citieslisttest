package com.test.sergeyklymenko.citylisttest;

import com.test.sergeyklymenko.citylisttest.data.entities.City;

import java.util.ArrayList;
import java.util.List;

public final class TestUtil {

    public static List<City> getCities() {

        List<City> cities = new ArrayList<>();

        City city1 = new City();
        city1.setCityName("Test 1");
        city1.setCount(10345);

        City city2 = new City();
        city2.setCityName("Test 3");
        city2.setCount(103454);

        cities.add(city1);
        cities.add(city2);

        return cities;
    }

    private TestUtil() {
        throw new UnsupportedOperationException();
    }
}
