package com.test.sergeyklymenko.citylisttest;

import com.test.sergeyklymenko.citylisttest.network.api.Api;
import com.test.sergeyklymenko.citylisttest.data.entities.CitiesResponse;
import com.test.sergeyklymenko.citylisttest.data.entities.City;
import com.test.sergeyklymenko.citylisttest.data.CitiesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CitiesRepositoryTest {

    @Mock
    Api api;

    private CitiesRepository repository;
    private TestSubscriber<List<City>> testSubscriber;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        repository = new CitiesRepository(api);
        testSubscriber = new TestSubscriber<>();
    }

    @Test
    public void getCitiesTest() {
        CitiesResponse response = new CitiesResponse();
        response.setResults(TestUtil.getCities());
        when(api.getCities()).thenReturn(Flowable.just(response));

        repository.getCities().subscribe(testSubscriber);
        verify(api).getCities();
        testSubscriber.assertValue(response.getResults());
    }
}
