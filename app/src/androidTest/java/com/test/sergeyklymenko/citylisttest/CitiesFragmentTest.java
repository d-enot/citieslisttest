package com.test.sergeyklymenko.citylisttest;

import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.test.sergeyklymenko.citylisttest.citieslist.CitiesActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CitiesFragmentTest {

    @Rule
    public ActivityTestRule<CitiesActivity> mActivityTestRule =
            new ActivityTestRule<>(CitiesActivity.class);

    @Test
    public void listNotEmpty() {
        IdlingResource idlingResourceSwipe = IdlingTestsResource
                .startTiming(Constants.IDLING_TIMEOUT);

        onView(withText(endsWith("Tasmania Region"))).check(matches(isDisplayed()));

        IdlingTestsResource.stopTiming(idlingResourceSwipe);
    }

    @Test
    public void listScrollable() {
        IdlingResource idlingResourceSwipe = IdlingTestsResource
                .startTiming(Constants.IDLING_TIMEOUT);

        onView(withId(R.id.list))
                .perform(RecyclerViewActions.scrollToPosition(18));

        IdlingTestsResource.stopTiming(idlingResourceSwipe);
    }
}
