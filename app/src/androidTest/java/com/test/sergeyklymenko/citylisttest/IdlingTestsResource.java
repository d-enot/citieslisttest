package com.test.sergeyklymenko.citylisttest;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;

public class IdlingTestsResource implements IdlingResource {

    private final long waitingTime;

    private long startTime;
    private ResourceCallback resourceCallback;

    public IdlingTestsResource(long mWaitingTime) {
        this.startTime = System.currentTimeMillis();
        this.waitingTime = mWaitingTime;
    }

    public static IdlingResource startTiming(long time) {
        IdlingResource idlingResource = new IdlingTestsResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public static void stopTiming(IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    @Override
    public String getName() {
        return IdlingTestsResource.class.getName() + ":" + waitingTime;
    }

    @Override
    public boolean isIdleNow() {
        long elapsed = System.currentTimeMillis() - startTime;
        boolean idle = (elapsed >= waitingTime);

        if (idle) {
            resourceCallback.onTransitionToIdle();
        }

        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        this.resourceCallback = callback;
    }
}
