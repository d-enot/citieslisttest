package com.test.sergeyklymenko.citylisttest.data;

import com.test.sergeyklymenko.citylisttest.data.entities.CitiesResponse;
import com.test.sergeyklymenko.citylisttest.network.api.Api;
import com.test.sergeyklymenko.citylisttest.data.entities.City;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class CitiesRepository {

    private Api api;

    @Inject
    public CitiesRepository(Api api) {
        this.api = api;
    }

    public Flowable<List<City>> getCities() {
        return api.getCities().map(CitiesResponse::getResults);
    }
}
