package com.test.sergeyklymenko.citylisttest;

public final class Constants {

    public static final String MAIN_URL = "https://api.openaq.org/v1/";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss Z";
    public static final int REQUEST_TIMEOUT = 30;
    public static final int CACHE_SIZE = 20 * 1024 * 1024;
    //for testing
    public static final int IDLING_TIMEOUT = 500;

    private Constants() {
        throw new UnsupportedOperationException();
    }
}
