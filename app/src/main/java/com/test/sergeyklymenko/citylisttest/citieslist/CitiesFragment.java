package com.test.sergeyklymenko.citylisttest.citieslist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.test.sergeyklymenko.citylisttest.R;
import com.test.sergeyklymenko.citylisttest.data.entities.City;
import com.test.sergeyklymenko.citylisttest.di.ActivityScoped;
import com.test.sergeyklymenko.citylisttest.dialog.ErrorDialogFragment;
import com.test.sergeyklymenko.citylisttest.network.connection.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

@ActivityScoped
public class CitiesFragment extends DaggerFragment implements CitiesContract.View {

    @Inject CitiesPresenter presenter;

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.progress_layout)
    LinearLayout mProgressLayout;

    @BindString(R.string.cities_fragment_error_dialog_txt)
    String errorTitle;

    @BindString(R.string.error_no_network_connections)
    String networkErrorMsg;

    @BindString(R.string.error_regular)
    String regularErrorMsg;

    @BindString(R.string.dialog_positive_btn)
    String positiveBtnTxt;

    @BindString(R.string.dialog_negative_btn)
    String negativeBtnTxt;

    private CitiesAdapter mAdapter;

    @Inject
    public CitiesFragment() {
        // Dagger requires empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cities, container, false);
        ButterKnife.bind(this, view);

        presenter.takeView(this);
        initList();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getCities();
    }

    @Override
    public void onDetach() {
        presenter.dropView();
        super.onDetach();
    }

    private void initList() {
        mAdapter = new CitiesAdapter(new ArrayList<>());
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(mAdapter);
    }

    @Override
    public void showLoading() {
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showCities(List<City> cities) {
        mAdapter.setBatch(cities);
    }

    @Override
    public void showError(@NonNull String error) {
        ErrorDialogFragment.show(getFragmentManager(),
                errorTitle,
                error + " " + regularErrorMsg,
                positiveBtnTxt,
                negativeBtnTxt,
                () -> presenter.getCities(),
                () -> getActivity().finish());
    }

    @Override
    public void showNetworkError() {
        ErrorDialogFragment.show(getFragmentManager(),
                errorTitle,
                networkErrorMsg,
                positiveBtnTxt,
                negativeBtnTxt,
                () -> presenter.getCities(),
                () -> getActivity().finish());
    }

    @Override
    public boolean networkEnabled() {
        return NetworkConnection.isNetworkEnabled(getContext());
    }
}
