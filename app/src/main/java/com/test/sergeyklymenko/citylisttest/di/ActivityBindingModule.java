package com.test.sergeyklymenko.citylisttest.di;

import com.test.sergeyklymenko.citylisttest.citieslist.CitiesActivity;
import com.test.sergeyklymenko.citylisttest.citieslist.CitiesModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = CitiesModule.class)
    abstract CitiesActivity citiesActivity();
}
