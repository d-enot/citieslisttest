package com.test.sergeyklymenko.citylisttest.citieslist;

import android.os.Bundle;

import com.test.sergeyklymenko.citylisttest.R;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerAppCompatActivity;

public class CitiesActivity extends DaggerAppCompatActivity {

    @Inject Lazy<CitiesFragment> fragmentProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);

        CitiesFragment citiesFragment =
                (CitiesFragment) getSupportFragmentManager().findFragmentById(R.id.frame);

        if (citiesFragment == null) {
            citiesFragment = fragmentProvider.get();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame, citiesFragment).commit();
        }
    }
}
