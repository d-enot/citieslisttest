package com.test.sergeyklymenko.citylisttest.citieslist;

import android.support.annotation.NonNull;

import com.test.sergeyklymenko.citylisttest.data.entities.City;

import java.util.List;

public interface CitiesContract {

    interface View {

        void showLoading();

        void hideLoading();

        void showCities(List<City> cities);

        void showNetworkError();

        void showError(@NonNull String error);

        boolean networkEnabled();
    }

    interface Presenter {

        void takeView(View view);

        void dropView();

        void getCities();

    }
}
