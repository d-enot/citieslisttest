package com.test.sergeyklymenko.citylisttest.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import io.reactivex.functions.Action;

public final class ErrorDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String TAG = ErrorDialogFragment.class.getName();
    private static final String TITLE_KEY = "title_key";
    private static final String MESSAGE_KEY = "message_key";
    private static final String POSITIVE_BTN_KEY = "positive_btn_key";
    private static final String NEGATIVE_BTN_KEY = "negative_btn_key";

    private Action positiveAction;
    private Action negativeAction;

    public static void show(@NonNull FragmentManager fragmentManager, @NonNull String title,
                            @NonNull String message, @NonNull String positiveBtn,
                            @NonNull Action positiveAction) {
        newInstance(title, message, positiveBtn, null, positiveAction, null)
                .show(fragmentManager.beginTransaction(), TAG);
    }

    public static void show(@NonNull FragmentManager fragmentManager, @NonNull String title,
                            @NonNull String message, @NonNull String positiveBtn,
                            @NonNull String negativeBtn, @NonNull Action positiveAction,
                            @NonNull Action negativeAction) {
        newInstance(title, message, positiveBtn, negativeBtn, positiveAction, negativeAction)
                .show(fragmentManager.beginTransaction(), TAG);
    }

    private static ErrorDialogFragment newInstance(@NonNull String title,
                                                   @NonNull String message,
                                                   @Nullable String positiveBtn,
                                                   @Nullable String negativeBtn,
                                                   @Nullable Action positiveAction,
                                                   @Nullable Action negativeAction) {

        Bundle args = new Bundle();
        args.putString(TITLE_KEY, title);
        args.putString(MESSAGE_KEY, message);
        args.putString(POSITIVE_BTN_KEY, positiveBtn);
        args.putString(NEGATIVE_BTN_KEY, negativeBtn);

        ErrorDialogFragment fragment = new ErrorDialogFragment();
        fragment.setArguments(args);
        fragment.positiveAction = positiveAction;
        fragment.negativeAction = negativeAction;
        fragment.setCancelable(false);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();

        String title = args.getString(TITLE_KEY);
        String message = args.getString(MESSAGE_KEY);
        String positiveBtn = args.getString(POSITIVE_BTN_KEY);
        String negativeBtn = args.getString(NEGATIVE_BTN_KEY);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(title);
        adb.setMessage(message);
        if (!TextUtils.isEmpty(positiveBtn)) {
            adb.setPositiveButton(positiveBtn, this);
        }
        if (!TextUtils.isEmpty(negativeBtn)) {
            adb.setNegativeButton(negativeBtn, this);
        }
        return adb.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {

            case DialogInterface.BUTTON_POSITIVE:

                if (positiveAction != null) {
                    try {
                        positiveAction.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case DialogInterface.BUTTON_NEGATIVE:

                if (negativeAction != null) {
                    try {
                        negativeAction.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public ErrorDialogFragment() {
    }
}
