package com.test.sergeyklymenko.citylisttest.di;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.test.sergeyklymenko.citylisttest.BuildConfig;
import com.test.sergeyklymenko.citylisttest.Constants;
import com.test.sergeyklymenko.citylisttest.network.api.Api;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public abstract class AppModule {

    @Binds
    abstract Context bindContext(Application application);

    @Provides
    @Singleton
    public static OkHttpClient provideHttpClient(@NonNull File cachedDir) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.cache(new Cache(cachedDir, Constants.CACHE_SIZE));
        httpClientBuilder.readTimeout(Constants.REQUEST_TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        httpClientBuilder.addInterceptor(interceptor);
        httpClientBuilder.addInterceptor(interceptor);
        return httpClientBuilder.build();
    }

    @Provides
    @Singleton
    public static Gson getMapper() {
        return new GsonBuilder().setDateFormat(Constants.DATE_FORMAT)
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .create();
    }

    @Provides
    @Singleton
    public static File getCacheDir(@NonNull Context context) {
        final File external = context.getExternalCacheDir();
        return external != null ? external : context.getCacheDir();
    }

    @Provides
    @Singleton
    public static Api providesApi(@NonNull OkHttpClient httpClient, @NonNull Gson mapper) {
        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(Constants.MAIN_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(mapper))
                .build()
                .create(Api.class);
    }
}

