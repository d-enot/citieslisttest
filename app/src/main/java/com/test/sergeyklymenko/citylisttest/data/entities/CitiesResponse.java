package com.test.sergeyklymenko.citylisttest.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class CitiesResponse {

    @SerializedName("results")
    private List<City> results;
}
