package com.test.sergeyklymenko.citylisttest;

import com.test.sergeyklymenko.citylisttest.di.AppComponent;
import com.test.sergeyklymenko.citylisttest.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}
