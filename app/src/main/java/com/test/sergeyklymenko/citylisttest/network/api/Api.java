package com.test.sergeyklymenko.citylisttest.network.api;


import com.test.sergeyklymenko.citylisttest.data.entities.CitiesResponse;

import io.reactivex.Flowable;
import retrofit2.http.GET;

public interface Api {

    @GET("cities")
    Flowable<CitiesResponse> getCities();
}
