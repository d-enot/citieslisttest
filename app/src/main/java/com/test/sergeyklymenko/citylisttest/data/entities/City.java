package com.test.sergeyklymenko.citylisttest.data.entities;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class City {

    @SerializedName("city")
    private String cityName;

    @SerializedName("count")
    private int count;
}
