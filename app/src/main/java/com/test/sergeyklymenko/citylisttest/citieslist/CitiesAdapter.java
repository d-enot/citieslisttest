package com.test.sergeyklymenko.citylisttest.citieslist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.sergeyklymenko.citylisttest.R;
import com.test.sergeyklymenko.citylisttest.data.entities.City;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private List<City> items;

    public CitiesAdapter(List<City> items) {
        this.items = items;
    }

    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city,
                parent, false));
    }

    @Override
    public void onBindViewHolder(CitiesAdapter.ViewHolder holder, int position) {
        City city = items.get(position);

        holder.cityName.setText(city.getCityName());
        holder.cityCount.setText(String.format(holder.countTxt, city.getCount()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setBatch(List<City> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.city_name_txt)
        TextView cityName;

        @BindView(R.id.count_txt)
        TextView cityCount;

        @BindString(R.string.cities_fragment_count_txt_label)
        String countTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
