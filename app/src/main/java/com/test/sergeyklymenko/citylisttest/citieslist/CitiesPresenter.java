package com.test.sergeyklymenko.citylisttest.citieslist;

import com.test.sergeyklymenko.citylisttest.data.CitiesRepository;
import com.test.sergeyklymenko.citylisttest.di.ActivityScoped;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@ActivityScoped
public class CitiesPresenter implements CitiesContract.Presenter {

    private CitiesRepository getCitiesUseCase;
    private CitiesContract.View view;
    private CompositeDisposable disposable;

    @Inject
    public CitiesPresenter(CitiesRepository getCitiesUseCase) {
        this.getCitiesUseCase = getCitiesUseCase;
        disposable = new CompositeDisposable();
    }

    @Override
    public void takeView(CitiesContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    public void getCities() {

        if (!view.networkEnabled()) {
            view.showNetworkError();
            return;
        }

        view.showLoading();
        disposable.add(getCitiesUseCase.getCities()
                .flatMap(Flowable::fromIterable)
                .filter(item -> item.getCount() > 10000)
                .toList()
                .toFlowable()
                .map(list -> {
                    Collections.sort(list, (a1, a2) -> a2.getCount() - a1.getCount());
                    return list;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> view.hideLoading())
                .subscribe(data -> view.showCities(data),
                        error -> view.showError(error.getMessage())));
    }
}
