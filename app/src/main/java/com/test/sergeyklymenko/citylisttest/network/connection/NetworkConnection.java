package com.test.sergeyklymenko.citylisttest.network.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *
 * @author Sergey Klymenko
 *
 * @since 1.0
 *
 */
public final class NetworkConnection {

    public static boolean isNetworkEnabled(Context context) {
        if (context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        if (cm == null) {
            return false;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private NetworkConnection() {
        throw new UnsupportedOperationException();
    }
}
