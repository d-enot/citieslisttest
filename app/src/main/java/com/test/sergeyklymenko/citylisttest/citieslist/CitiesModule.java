package com.test.sergeyklymenko.citylisttest.citieslist;

import com.test.sergeyklymenko.citylisttest.di.ActivityScoped;
import com.test.sergeyklymenko.citylisttest.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CitiesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CitiesFragment imporFragment();

    @ActivityScoped
    @Binds
    abstract CitiesContract.Presenter contactsPresenter(CitiesPresenter contactsPresenter);
}
