# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is test exemple

### Libreries ###

* RxJava2 - streaming and thread management
* Dagger2 - dependency injection
* Butterknife - reduce boilerplate code for getViewById
* Lombock - reduce boilerplate code for getters, setters, hashCode, equals, toString
* okhttp - HTTP & HTTP/2 client
* retrofit2 - networking requests
* Mockito - testing
* Esppresso - testing